const axios = require('axios')
const path = require('path')
const fs = require('fs')
const mustache = require('mustache')
var Ajv = require('ajv');
const jsonFormat = require('json-format')
const _ = require('lodash')

const resolveJsonSchema = (fileRelativePath)  => {
  return new Promise((resolve, reject) => {
    const filePath = path.resolve(__dirname, fileRelativePath)
    fs.readFile(filePath, (err, bufferContent) => {
      if (err) {
        return reject({error: "Erro lendo arquivo" + fileRelativePath, detail: err})
      }
      resolve(JSON.parse(bufferContent.toString()))
    })
  })
}

step("valide resposta da requisicao <requestAlias> contra o schema descrito em <schemaPath>", async (requestAlias, schemaPath) => {
  const schemaContent = await resolveJsonSchema(schemaPath)
  const ajv = new Ajv({allErrors: true})
  const validate = ajv.compile(schemaContent);
  const requestResponse = this.requestResultData[requestAlias]
  const valid = validate(requestResponse)
  if (!valid) {
    throw new Error(`Requisicao ${requestAlias} falhou na validação de schema ${schemaPath}` + jsonFormat(validate.errors))
  }
})

const resolveReqFile = (fileRelativePath, vars = {})  => {
  return new Promise((resolve, reject) => {
    const filePath = path.resolve(__dirname, fileRelativePath)
    fs.readFile(filePath, (err, bufferContent) => {
      if (err) {
        return reject({error: "Erro lendo arquivo" + fileRelativePath, detail: err})
      }
      resolve(JSON.parse(mustache.render(bufferContent.toString(), vars)))
    })
  })
}


step('Usando variavel <variable>:<type> com valor <value>', async (variable, type, value) => {
  let valueToType = convertValueToType(type, value);
  if (!this.vars) {
    this.vars = {
      [variable]: valueToType
    }
  } else {
    this.vars = Object.assign(this.vars, {
      [variable]: valueToType
    })
  }
})

step("Utilizando requisicao descrita em <arg0> com alias <alias>", async (nomearquivo, alias) => {
  this[alias] = await resolveReqFile(nomearquivo, this.vars || {})
})

step("Execute a requisiçao <requestAlias>", async (requestAlias) => {
  const req = this[requestAlias]
  const resource = `${req.protocol}://${req.host}:${req.port || 80}${req.path}`
  try {
    const requestResult = await axios.request({
      method: `${req.method}`.toLowerCase(),
      url: resource,
      headers: req.headers,
      data: req.method !== 'get' ? req.data : null
    })
    this.requestResultData = Object.assign(this.requestResultData || {}, {
      [requestAlias]: requestResult.data
    })
    this.requestResultHeaders = Object.assign(this.requestResultHeaders || {}, {
      [requestAlias]: requestResult.headers
    })
  } catch (error) {
    throw error
  }
})

step("Valide que em <requestAlias> o path <path> é <comparator> <actual>:<actualType>", async (requestAlias, path, comparator, actual, actualType) => {
  const comparatorDict = {
    "igual a": (a, b) => a === b,
    "diferente de": (a, b) => a !== b,
    "maior que": (a, b) => a > b,
    "menor que": (a, b) => a > b
  }
  const comparatorInUse = comparatorDict[comparator]
  const valueAtPath = _.get(this.requestResultData[requestAlias], path)
  const actualForcedToType = convertValueToType(actualType, actual)
  const comparisonResult = comparatorInUse(valueAtPath, actualForcedToType)
  if (!comparisonResult) {
    throw new Error(`Assercao falhou para \t ${valueAtPath} [${comparator}] ${actualForcedToType}`)
  }
})

step("Imprimir variaveis", async () => {
  console.log('\nVariaveis disponiveis')
  console.log( this.vars, '\n')
})

step("Imprimir requisicao", async () => {
  console.log('\nDUMP da requisicao')
  console.log( this.requestDescription, '\n')
})

function convertValueToType(type, value) {
  if (type === 'string' || !type) {
    return `${value}`;
  }
  if (type === 'number') {
    return Number(value);
  }
  if (type === 'array' || type === 'object' || type === 'boolean') {
    return JSON.parse(value);
  }
  return value;
}
