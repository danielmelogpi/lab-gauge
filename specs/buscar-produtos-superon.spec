# Buscar formas de pgamento no superon

## Buscar formas de pgamento simples
* Usando variavel "host":"string" com valor "superon.lifeapps.com.br"
* Utilizando requisicao descrita em "requisicao/requer-lista-forma-pagamento.request.json" com alias "requestFormaPagamento"
* Execute a requisiçao "requestFormaPagamento"
  * Valide que em "requestFormaPagamento" o path "0.idformapagto" é "igual a" "9c1333ff-c617-47d6-baad-0fbd3a805e13":"string"
  
## Buscar formas de pagamento com schema
* Usando variavel "host":"string" com valor "superon.lifeapps.com.br"
* Utilizando requisicao descrita em "requisicao/requer-lista-forma-pagamento.request.json" com alias "requestFormaPagamento"
* Execute a requisiçao "requestFormaPagamento"
* Valide que em "requestFormaPagamento" o path "0.idformapagto" é "igual a" "9c1333ff-c617-47d6-baad-0fbd3a805e13":"string"
* valide resposta da requisicao "requestFormaPagamento" contra o schema descrito em "json-schema/lista-formas-pagamento.schema.json"
